using System.Threading.Tasks;
using R2.Framework.Infrastrcture.Message;

namespace R2.Framework.Infrastrcture.Bus
{
    public interface IEventBus
    {
        Task Publish<TEvent>(TEvent @event)
        where TEvent : class, IEvent;
    }

}