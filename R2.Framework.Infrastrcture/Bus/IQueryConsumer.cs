using System.Threading.Tasks;
using R2.Framework.Infrastrcture.Message;

namespace R2.Framework.Infrastrcture.Bus
{
    public interface IQueryConsumer<in TQuery, TQueryResponse>
        where TQuery : class, IQuery
        where TQueryResponse : class, IMessage
    {
        Task<TQueryResponse> Consume(TQuery query);
    }

}