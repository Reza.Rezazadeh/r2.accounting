using System.Threading.Tasks;
using R2.Framework.Infrastrcture.Message;

namespace R2.Framework.Infrastrcture.Bus {

    // public abstract class ListQueryHandler<TCommand, TCommandResponse> : ICommandConsumer<TCommand, TCommandResponse>
    //     where TCommand : class, ICommand
    // where TCommandResponse : class {
    //     public virtual Task<TCommandResponse> Consume (TCommand command) {
    //         return null;
    //     }
    // }
    public abstract class MessageHandler<TCommand, TCommandResponse> : ICommandConsumer<TCommand, TCommandResponse>
        where TCommand : class, ICommand
    where TCommandResponse : class {
        public virtual Task<TCommandResponse> Consume (TCommand command) {
            return null;
        }
    }
    public interface ICommandConsumer<in TCommand, TCommandResponse>
        where TCommand : class, ICommand
    where TCommandResponse : class {
        Task<TCommandResponse> Consume (TCommand command);
    }

    public interface ICommandConsumer<in TCommand>
        where TCommand : class, ICommand {
            Task Consume (TCommand command);
        }

}