using System.Threading.Tasks;
using R2.Framework.Infrastrcture.Message;

namespace R2.Framework.Infrastrcture.Bus
{
    public interface IEventConsumer<in TEvent>
        where TEvent : IEvent
    {
        Task Consume(TEvent @event);
    }

}