﻿using System.Threading.Tasks;
using R2.Framework.Infrastrcture.Message;

namespace R2.Framework.Infrastrcture.Bus
{
    public interface ICommandBus
    {
        Task<TCommandResponse> Send<TCommand, TCommandResponse>(TCommand command)
            where TCommand : class, ICommand
            where TCommandResponse : class, IMessage;
    }



}