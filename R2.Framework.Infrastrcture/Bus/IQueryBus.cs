using System.Threading.Tasks;
using R2.Framework.Infrastrcture.Message;

namespace R2.Framework.Infrastrcture.Bus
{
    public interface IQueryBus
    {
        Task<TQueryResponse> Send<TQuery, TQueryResponse>(TQuery request)
            where TQuery : class, IQuery
            where TQueryResponse : class, IMessage;
    }

}