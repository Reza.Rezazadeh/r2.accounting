using System.Collections.Generic;

namespace R2.Framework.Infrastrcture.Message {
    public abstract class QueryResult : Result { }

    public abstract class SingleQueryResult<T> : QueryResult {
        public T Entity { get; set; }
    }

    public class ListQueryResult<T> : QueryResult {
        public int TotalCount { get; set; }
        public ICollection<T> Entities { get; set; }
    }

}