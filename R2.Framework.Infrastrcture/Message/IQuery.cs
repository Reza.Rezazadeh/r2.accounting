namespace R2.Framework.Infrastrcture.Message {
    public interface IQuery
        : IMessage { }

    public interface IListQuery : IQuery {
        int Skip { get; set; }
        int Take { get; set; }
    }

    public interface IListQuery<TResponse>
        : IQuery where TResponse : IResult { }

}