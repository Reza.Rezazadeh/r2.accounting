namespace R2.Framework.Infrastrcture.Message
{
    public abstract class ListRequest<TResponse> : IListQuery<TResponse> where TResponse : ListQueryResult<TResponse> {
        public ListRequest () {
            Skip = 0;
            Take = int.MaxValue;
        }
        public int Skip { get; set; }
        public int Take { get; set; }
        public bool NeedsReply { get; set; }

    }
}