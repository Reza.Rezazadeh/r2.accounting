namespace R2.Framework.Infrastrcture.Message {
    public interface ICommand
        : IMessage { }
    public interface IRequest
        : ICommand { }
    public interface IRequest<TResponse>
        : ICommand where TResponse : IResult { }

}