using System.Collections.Generic;
using FluentValidation.Results;

namespace R2.Framework.Infrastrcture.Message {

    public interface IResult : IMessage {
        bool Succeeded { get; set; }
        string Message { get; set; }
        string ErrorCode { get; set; }
        List<ValidationFailure> ValidationFailures { get; set; }
    }
    public class Result : IResult {
        public Result () {
            Succeeded = true;
            ValidationFailures = new List<ValidationFailure>();
            StatusCode = 200;
        }

        public bool Succeeded { get; set; }
        public string Message { get; set; }
        public string ErrorCode { get; set; }
        public int StatusCode { get; set; }
        public List<ValidationFailure> ValidationFailures { get; set; }

    }

}