namespace R2.Framework.Infrastrcture.Message
{
    public abstract class Request<TResponse> : IRequest<TResponse> where TResponse : Result {
        public bool NeedsReply { get; set; }
    }
}