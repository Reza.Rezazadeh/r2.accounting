using System;

namespace R2.Framework.Infrastrcture.Decorators {

    [AttributeUsage (AttributeTargets.Class, Inherited = false, AllowMultiple = true)]
    public sealed class AuditLogAttribute : Attribute {
        public AuditLogAttribute () { }
    }
}