﻿using System;
using System.Threading.Tasks;
using Newtonsoft.Json;
using R2.Framework.Infrastrcture.Bus;
using R2.Framework.Infrastrcture.Message;

namespace R2.Framework.Infrastrcture.Decorators {

    public sealed class AuditLoggingDecorator<TCommand, TOutput> : ICommandConsumer<TCommand, TOutput>
        where TCommand : class, ICommand
    where TOutput : class {
        private readonly ICommandConsumer<TCommand, TOutput> _handler;

        public AuditLoggingDecorator (ICommandConsumer<TCommand, TOutput> handler) {
            _handler = handler;
        }
        public Task<TOutput> Consume (TCommand command) {
            string commandJson = JsonConvert.SerializeObject (command);

            Console.WriteLine ($"Command of type {command.GetType().Name}: {commandJson}");

            return _handler.Consume (command);
        }
    }
}