﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using R2.Framework.Domain.Entity;

namespace R2.Accounting.Domain
{

    public class TransactionCategory : TAggregate
    {
        public string Name { get; set; }
        public string Icon { get; set; }
        public bool IsSystemType { get; set; }
        public virtual TransactionType TransactionType { get; set; }

        public TransactionCategory ParentCategory { get; set; }
        public ICollection<TransactionCategory> ChildCategories { get; set; }

    }

    public class TransactionCategoryTypeConfiguration : IEntityTypeConfiguration<TransactionCategory>
    {
        public void Configure(EntityTypeBuilder<TransactionCategory> builder)
        {
            builder.HasMany(c => c.ChildCategories).WithOne(c => c.ParentCategory);

            var predefinedData = new List<TransactionCategory>
            {
                new TransactionCategory { Id = Guid.NewGuid(), Name = "افتتاح حساب", IsSystemType = true, TransactionType = TransactionType.Income }
            };
            builder.HasData(predefinedData);
        }
    }

}