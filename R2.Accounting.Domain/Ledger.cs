using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using R2.Framework.Domain.Entity;
using R2.Framework.Domain.Entity.Auditing;

namespace R2.Accounting.Domain {
    public class Ledger : TAggregate, IAudited<User>, IDeletionAudited<User>, ISoftDelete {

        public string Name { get; set; }
        public decimal Balance { get; private set; }
        public virtual ICollection<Account> Accounts { get; set; }
        public virtual ICollection<LedgerAccessRight> LedgerAccesses { get; set; }
        public User CreatorUser { get; set; }
        public DateTime CreationTime { get; set; }
        public User LastModifierUser { get; set; }
        public DateTime? LastModificationTime { get; set; }
        public bool IsDeleted { get; set; }
        public User DeleterUser { get; set; }
        public DateTime? DeletionTime { get; set; }

        public void BalanceCaliculation () {
            Balance = Accounts.Where (c => c.IsDeleted == false).Sum (c => c.Balance);
        }

        public void AddPermission (User user, LedgerPermission permission) {
            if (LedgerAccesses == null) {
                LedgerAccesses = new List<LedgerAccessRight> ();
            }
            LedgerAccesses.Add (new LedgerAccessRight {
                Ledger = this,
                    User = user,
                    LedgerPermission = permission
            });
        }

        public void RemovePermission (LedgerAccessRight ledgerAccessRight) {
            var accessRight = LedgerAccesses.FirstOrDefault (c => c.Id == ledgerAccessRight.Id);
            if (LedgerAccesses == null || accessRight == null) {
                return;
            }
            LedgerAccesses.Remove (ledgerAccessRight);
        }
    }

    public class LedgerTypeConfiguration : IEntityTypeConfiguration<Ledger> {
        public void Configure (EntityTypeBuilder<Ledger> builder) {
            builder.HasMany (c => c.Accounts).WithOne (c => c.Ledger);
            builder.HasMany (c => c.LedgerAccesses).WithOne (c => c.Ledger);
        }
    }
}