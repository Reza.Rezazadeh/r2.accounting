using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using R2.Framework.Domain.Entity;
using R2.Framework.Domain.Entity.Auditing;

namespace R2.Accounting.Domain {
    public class LedgerAccessRight : TAggregate {
        public virtual Ledger Ledger { get; set; }
        public virtual User User { get; set; }
        public LedgerPermission LedgerPermission { get; set; }
    }

    public enum LedgerPermission {
        [Display (Name = "نمایش")]
        View,
        [Display (Name = "نمایش و ویرایش")]
        Edit
    }
}