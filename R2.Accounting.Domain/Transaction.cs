using R2.Framework.Domain.Entity;
using R2.Framework.Domain.Entity.Auditing;
using System;

namespace R2.Accounting.Domain
{
    public class Transaction : TAggregate, IAudited<User>, IDeletionAudited<User>
    {
        public virtual Account Account { get; set; }
        public virtual TransactionType TransactionType { get; set; }
        public decimal Value { get; set; }
        public DateTime TransactionDateTime { get; set; }
        public string Note { get; set; }
        public TransactionCategory Category { get; set; }
        public User CreatorUser { get; set; }
        public DateTime CreationTime { get; set; }
        public User LastModifierUser { get; set; }
        public DateTime? LastModificationTime { get; set; }
        public User DeleterUser { get; set; }
        public DateTime? DeletionTime { get; set; }
        public bool IsDeleted { get; set; }
    }
}