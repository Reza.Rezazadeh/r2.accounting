using System;
using R2.Framework.Domain.Entity;
using R2.Framework.Domain.Entity.Auditing;

namespace R2.Accounting.Domain
{
    public class EventSource : TAggregate, ICreationAudited<User>
    {
        public string RequestType { get; set; }
        public string Request { get; set; }
        public bool Succeeded { get; set; }
        public string Response { get; set; }
        public User CreatorUser { get; set; }
        public DateTime CreationTime { get; set; }
        public long Duaration { get; set; }
    }
}