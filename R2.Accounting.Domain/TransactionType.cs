using System.ComponentModel.DataAnnotations;

namespace R2.Accounting.Domain
{
    public enum TransactionType
    {
        [Display(Name="دریافتی")]
        Income,
        [Display(Name="پرداختی")]
        Expense,
        [Display(Name="انتقال")]
        Transfer
    }
}