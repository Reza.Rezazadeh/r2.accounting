using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using R2.Framework.Domain.Entity;
using R2.Framework.Domain.Entity.Auditing;
using R2.Framework.Domain.Exceptions;

namespace R2.Accounting.Domain
{

    public class Account : TAggregate, IAudited<User>, IDeletionAudited<User>
    {
        public virtual Ledger Ledger { get; set; }
        public string Name { get; set; }
        public string AccountNumber { get; set; }
        public string CardNumber { get; set; }
        public decimal Balance { get; private set; }
        public virtual ICollection<Transaction> Transactions { get; set; }
        public User CreatorUser { get; set; }
        public DateTime CreationTime { get; set; }
        public User LastModifierUser { get; set; }
        public DateTime? LastModificationTime { get; set; }
        public DateTime? DeletionTime { get; set; }
        public User DeleterUser { get; set; }
        public bool IsDeleted { get; set; }

        public Transaction GetTransactionById(Guid transactionId)
        {
            return Transactions.FirstOrDefault(c => c.Id == transactionId);
        }

        public IQueryable<Transaction> GetTransactions(bool showDeleted = false)
        {
            if (showDeleted)
            {
                return Transactions.AsQueryable();
            }
            else
            {
                return Transactions.Where(c => c.IsDeleted == false).AsQueryable();
            }
        }

        public void AddTransaction(Transaction transaction)
        {
            if (!Transactions.Any(c => c.Id == transaction.Id))
            {
                transaction.Account = this;
                transaction.CreationTime = DateTime.UtcNow;
                Transactions.Add(transaction);

                BalanceCaliculation();
            }
            else
            {
                throw new Exception("Duplicated !");
            }
        }

        public void RemoveTransaction(Guid transactionId)
        {
            var transaction = Transactions.FirstOrDefault(c => c.Id == transactionId);
            if (transaction != null)
            {
                transaction.IsDeleted = true;
                transaction.DeletionTime = DateTime.UtcNow;

                BalanceCaliculation();
            }
            else
            {
                throw new EntityNotFoundException("Entity Not Found!");
            }
        }
        public void UpdateTransaction(Transaction transaction)
        {
            var entity = Transactions.FirstOrDefault(c => c.Id == transaction.Id);
            if (entity != null)
            {
                entity = transaction;
                entity.LastModificationTime = DateTime.UtcNow;
                BalanceCaliculation();
            }
            else
            {
                throw new EntityNotFoundException("Entity Not Found!");
            }
        }

        public void BalanceCaliculation()
        {

            var income = GetTransactions().Where(c => c.TransactionType == TransactionType.Income).Sum(c => c.Value);
            var expens = GetTransactions().Where(c => c.TransactionType != TransactionType.Income).Sum(c => c.Value);
            Balance = income - expens;
            Ledger.BalanceCaliculation();
        }
    }

    public class AccountTypeConfiguration : IEntityTypeConfiguration<Account>
    {
        public void Configure(EntityTypeBuilder<Account> builder)
        {
            builder.HasMany(c => c.Transactions).WithOne(c => c.Account);
        }
    }
}