using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace R2.Accounting.Domain.Repository {
    public interface IUserRepository {
        Task<User> GetUserByName (string userName);
        Task<User> GetUser (Guid id);
    }
    public class UserRepository : IUserRepository {
        private readonly DbContext _dbContext;
        public DbSet<User> Entity;

        public UserRepository (DbContext dbContext) {
            _dbContext = dbContext ??
                throw new ArgumentNullException (nameof (dbContext));
            Entity = _dbContext.Set<User> ();
        }
        public Task<User> GetUser (Guid id) {
            return Entity.FirstOrDefaultAsync (c => c.Id == id.ToString());
        }

        public Task<User> GetUserByName (string userName) {
            return Entity.FirstOrDefaultAsync (c => c.UserName == userName);
        }
    }
}