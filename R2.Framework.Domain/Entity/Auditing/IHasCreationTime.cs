
using System;

namespace R2.Framework.Domain.Entity.Auditing
{
    public interface IHasCreationTime
    {
        DateTime CreationTime { get; set; }
    }

    public interface ICreationAudited<TUser> : IHasCreationTime
    {
        /// <summary>
        /// Reference to the creator user of this entity.
        /// </summary>
        TUser CreatorUser { get; set; }
    }
    
   
}
