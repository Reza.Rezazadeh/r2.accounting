namespace R2.Framework.Domain.Entity.Auditing {

    public interface IAudited<TUser> : ICreationAudited<TUser>, IModificationAudited<TUser> {

    }
}