
using System;

namespace R2.Framework.Domain.Entity.Auditing
{
    public interface IHasModificationTime
    {
        DateTime? LastModificationTime { get; set; }
    }

    public interface IModificationAudited<TUser> : IHasModificationTime
    {
        TUser LastModifierUser { get; set; }
    }
}
