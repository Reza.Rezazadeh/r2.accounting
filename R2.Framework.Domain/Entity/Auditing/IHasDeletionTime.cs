
using System;

namespace R2.Framework.Domain.Entity.Auditing
{
    public interface IHasDeletionTime : ISoftDelete
    {
        DateTime? DeletionTime { get; set; }
    }

    public interface IDeletionAudited<TUser> : IHasDeletionTime
    {
        TUser DeleterUser { get; set; }
    }
}
