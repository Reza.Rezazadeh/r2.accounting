namespace R2.Framework.Domain.Entity
{
    public interface IPassivable
    {
        bool IsActive { get; set; }
    }
}
