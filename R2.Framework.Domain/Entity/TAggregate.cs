﻿using System;
using System.ComponentModel.DataAnnotations;

namespace R2.Framework.Domain.Entity
{
    public abstract class TAggregate
    {
        public Guid Id { get; set; }
        [Timestamp]
        public byte[] ConcurrencyStamp { get; set; }
    }
}
