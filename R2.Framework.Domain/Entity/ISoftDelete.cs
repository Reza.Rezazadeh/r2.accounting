namespace R2.Framework.Domain.Entity
{
    public interface ISoftDelete
    {
        bool IsDeleted { get; set; }
    }
}
