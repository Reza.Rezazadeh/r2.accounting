﻿using Microsoft.EntityFrameworkCore;
using R2.Framework.Domain.Helpers;

namespace R2.Framework.Domain.Context
{
    public interface IDataContext {
        int SaveChanges ();
        void Dispose ();
    }

    public class DataContext : DbContext, IDataContext {
        public DataContext () : base () { }
        public DataContext (DbContextOptions<DataContext> options) : base (options) {

        }

        protected override void OnModelCreating (ModelBuilder modelBuilder) {
            modelBuilder.DefineTAggregateDbSets ();
            modelBuilder.UseEntityTypeConfiguration ();

            base.OnModelCreating (modelBuilder);
        }
    }
}