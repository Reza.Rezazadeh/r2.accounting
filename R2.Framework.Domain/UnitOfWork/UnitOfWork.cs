using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using R2.Framework.Domain.Entity;
using R2.Framework.Domain.Repository;

namespace R2.Framework.Domain.Context
{
    public class UnitOfWork<TContext> : IRepositoryFactory, IUnitOfWork<TContext>, IUnitOfWork where TContext : DbContext {
        private bool disposed = false;
        private Dictionary<Type, object> repositories;

        /// <summary>
        /// Initializes a new instance of the <see cref="UnitOfWork{TContext}"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        public UnitOfWork (TContext context) {
            DbContext = context ??
                throw new ArgumentNullException (nameof (context));
        }

        /// <summary>
        /// Gets the db context.
        /// </summary>
        /// <returns>The instance of type <typeparamref name="TContext"/>.</returns>
        public TContext DbContext { get; }

        /// <summary>
        /// Gets the specified repository for the <typeparamref name="TEntity"/>.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <returns>An instance of type inherited from <see cref="IRepository{TEntity}"/> interface.</returns>
        public IRepository<TEntity> GetRepository<TEntity> () where TEntity : TAggregate {
            if (repositories == null) {
                repositories = new Dictionary<Type, object> ();
            }

            var type = typeof (TEntity);
            if (!repositories.ContainsKey (type)) {
                repositories[type] = new Repository<TEntity> (DbContext);
            }

            return (IRepository<TEntity>) repositories[type];
        }

        /// <summary>
        /// Executes the specified raw SQL command.
        /// </summary>
        /// <param name="sql">The raw SQL.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns>The number of state entities written to database.</returns>
        public int ExecuteSqlRaw (string sql, params object[] parameters) {
            return DbContext.Database.ExecuteSqlRaw (sql, parameters);
        }

        /// <summary>
        /// Uses raw SQL queries to fetch the specified <typeparamref name="TEntity" /> data.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="sql">The raw SQL.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns>An <see cref="IQueryable{T}" /> that contains elements that satisfy the condition specified by raw SQL.</returns>
        public IQueryable<TEntity> FromSql<TEntity> (string sql, params object[] parameters) where TEntity : TAggregate =>
            DbContext.Set<TEntity> ().FromSqlRaw (sql, parameters);

        /// <summary>
        /// Saves all changes made in this context to the database.
        /// </summary>
        /// <param name="ensureAutoHistory"><c>True</c> if save changes ensure auto record the change history.</param>
        /// <returns>The number of state entries written to the database.</returns>
        public (int, byte[], string) SaveChanges (bool ensureAutoHistory = false) {
            if (ensureAutoHistory) {
                DbContext.EnsureAutoHistory ();
            }
            try {
                return (DbContext.SaveChanges (), null, string.Empty);
            } catch (DbUpdateConcurrencyException ex) {
                (byte[] RowVersion, string Message) = DbUpdateConcurrencyExceptionHandler (ex.Entries);
                return (-1, RowVersion, Message);
            }
        }

        /// <summary>
        /// Asynchronously saves all changes made in this unit of work to the database.
        /// </summary>
        /// <param name="ensureAutoHistory"><c>True</c> if save changes ensure auto record the change history.</param>
        /// <returns>A <see cref="Task{TResult}"/> that represents the asynchronous save operation. The task result contains the number of state entities written to database.</returns>
        public async Task < (int, byte[] , string) > SaveChangesAsync (bool ensureAutoHistory = false) {
            if (ensureAutoHistory) {
                DbContext.EnsureAutoHistory ();
            }

            try {
                var count = await DbContext.SaveChangesAsync ();
                return (count, null, string.Empty);

            } catch (DbUpdateConcurrencyException ex) {
                (byte[] RowVersion, string Message) = DbUpdateConcurrencyExceptionHandler (ex.Entries);
                return (-1, RowVersion, Message);
            }
        }

        /// <summary>
        /// Saves all changes made in this context to the database with transaction.
        /// </summary>
        /// <param name="ensureAutoHistory"><c>True</c> if save changes ensure auto record the change history.</param>
        /// <returns>A <see cref="Task{TResult}"/> that represents the asynchronous save operation. The task result contains the number of state entities written to database.</returns>
        public async Task<int> CommitAsync (bool ensureAutoHistory = false) {
            // TransactionScope will be included in .NET Core v2.0
            using var transaction = DbContext.Database.BeginTransaction();
            try
            {
                var count = await SaveChangesAsync(ensureAutoHistory);
                transaction.Commit();
                return count.Item1;
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                throw ex;
            }
        }

        private (byte[], string) DbUpdateConcurrencyExceptionHandler (IReadOnlyList<EntityEntry> entries) {
            var error = new StringBuilder ();
            if (entries.Count == 1) {
                var entry = entries[0];
                var proposedValues = entry.CurrentValues;
                var databaseValues = entry.GetDatabaseValues ();

                foreach (var property in proposedValues.Properties) {
                    var proposedValue = proposedValues[property];
                    var databaseValue = databaseValues[property];
                    if (proposedValue != databaseValue)
                        error.AppendLine ($"Current value '{nameof(property)}': {databaseValue}");
                }
                var rowVersion = databaseValues["RowVersion"] as byte[];
                return (rowVersion, error.ToString ());
            }
            return (new byte[] { }, string.Empty);

        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose () {
            Dispose (true);

            GC.SuppressFinalize (this);
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        /// <param name="disposing">The disposing.</param>
        protected virtual void Dispose (bool disposing) {
            if (!disposed) {
                if (disposing) {
                    // clear repositories
                    if (repositories != null) {
                        repositories.Clear ();
                    }

                    // dispose the db context.
                    DbContext.Dispose ();
                }
            }

            disposed = true;
        }

    }
}