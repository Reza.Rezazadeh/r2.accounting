using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using R2.Accounting.Domain;
using R2.Framework.Domain.Context;
using R2.Framework.Domain.Helpers;

namespace R2.Accounting.Test
{
    public class ShopNetDataContext : DbContext, IDataContext
    {
        private readonly IConfigurationRoot Configuration;
        public ShopNetDataContext(IConfigurationRoot configuration)
        {
            Configuration = configuration;
        }
        public ShopNetDataContext() { }
        public ShopNetDataContext(DbContextOptions<ShopNetDataContext> options) : base(options) { }

        public DbSet<EventSource> EventSources { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.DefineTAggregateDbSets();
            modelBuilder.UseEntityTypeConfiguration();

            modelBuilder.Entity<User>().HasData(IdentityService.UsersList());
            base.OnModelCreating(modelBuilder);
        }
    }
}