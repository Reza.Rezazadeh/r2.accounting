﻿using System;
using System.Collections.Generic;
using R2.Accounting.Domain;

namespace R2.Accounting.Test
{
    static class IdentityService
    {
        public static List<User> UsersList()
        {
            var users = new List<User>
            {
                new User
                {
                    Id = "11111111-1111-1111-1111-111111111111",
                    PhoneNumber = "1",
                    NickName = "User 1"
                },
                new User
                {
                    Id = "22222222-2222-2222-2222-222222222222",
                    PhoneNumber = "2",
                    NickName = "User 2"
                }
            };

            return users;
        }
        public static User Login(int index)
        {
            return UsersList()[index];
        }
    }
}