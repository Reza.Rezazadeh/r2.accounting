﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using R2.Accounting.Commands;
using R2.Accounting.Commands.Ledger;
using R2.Accounting.Test.Bus;
using R2.Framework.Infrastrcture.Bus;
using R2.Framework.Infrastrcture.Message;
using R2.Framework.MB.Configurations;
using R2.Framework.MB.Extensions;
using R2.Framework.MB.Interfaces;

namespace R2.Accounting.Test {

    [TestClass]
    public class RealBusTest {
        private ICommandBus Bus;

        [TestInitialize]
        public async Task InitBus () {
            ServiceCollection services = new ServiceCollection ();
            var rabbitMqOptions = new RabbitMqOptions {
                HostName = "rabbitmq://localhost",
                Username = "guest",
                Password = "guest",
                EndpointName = "R2.Bus.Test",
                AutoDelete = true
            };

            services.RegisterCommandSender (null, rabbitMqOptions);

            var container = services.BuildServiceProvider ();

            var _commandSender = container.GetService<ICommandSenderMessageBusManager> ();
            Bus = container.GetService<ICommandBus> ();
            await _commandSender.Start ();
        }

        [TestMethod]
        public async Task BusFullTest () {
            var result = await Bus.Send<CreateLedger, CreateLedgerResult> (new CreateLedger {
                CreatorUser = new Guid("0b6967bf-1234-45b8-87b8-8c9da66cbfd5"),
                Name = "Leadger From Test"
            });
            Assert.IsNotNull(result.Entity);
        }

    }
}