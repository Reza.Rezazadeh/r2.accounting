using System;
using R2.Framework.Infrastrcture.Message;

namespace R2.Accounting.Test.Bus {

    public class FakeCommand : Request<FakeResponse> {
        public Guid Id { get; }

        public FakeCommand (Guid id) {
            Id = id;
        }
    }

    public class FakeResponse
        : Result {
            public Guid IdResponse { get; }

            public FakeResponse (Guid idResponse) {
                IdResponse = idResponse;
            }
        }

}