﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using R2.Accounting.Commands;
using R2.Accounting.Commands.Ledger;
using R2.Accounting.Test.Bus;
using R2.Framework.Infrastrcture.Bus;
using R2.Framework.Infrastrcture.Message;
using R2.Framework.MB.Configurations;
using R2.Framework.MB.Extensions;
using R2.Framework.MB.Interfaces;

namespace R2.Accounting.Test {

    [TestClass]
    public class BusTest {
        private ICommandBus Bus;

        [TestInitialize]
        public async Task InitBus () {
            ServiceCollection services = new ServiceCollection ();
            services.AddHandlers();
            var rabbitMqOptions = new RabbitMqOptions {
                HostName = "rabbitmq://localhost",
                Username = "guest",
                Password = "guest",
                EndpointName = "R2.Bus.Test",
                AutoDelete = true
            };
            services.RegisterCommandConsumer (commandOptions => {
                    commandOptions.ConsumesCommand<FakeCommand, FakeResponse> ();
                },
                rabbitMqOptions);

            services.RegisterCommandSender (null, rabbitMqOptions);

            var container = services.BuildServiceProvider ();

            var _commandConsumer = container.GetService<ICommandConsumerMessageBusManager> ();
            var _commandSender = container.GetService<ICommandSenderMessageBusManager> ();
            Bus = container.GetService<ICommandBus> ();
            await _commandConsumer.Start ();
            await _commandSender.Start ();

        }

        [TestMethod]
        public async Task BusFullTest () {
                
            var id = Guid.NewGuid ();
            var result = await Bus.Send<FakeCommand, FakeResponse> (new FakeCommand (id));
            Assert.IsNotNull(result.IdResponse);
        }

    }
}