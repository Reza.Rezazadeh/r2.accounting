using System.Threading.Tasks;
using R2.Framework.Infrastrcture.Bus;

namespace R2.Accounting.Test.Bus
{
    public class FakeCommandHandler
        : MessageHandler<FakeCommand, FakeResponse>
    {
        public override async Task<FakeResponse> Consume(FakeCommand command)
        {
            var id = command.Id;
            var response = new FakeResponse(id);
            return await Task.FromResult(response);
        }
    }

}