using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using R2.Accounting.Commands;
using R2.Accounting.Commands.Ledger;
using R2.Framework.Infrastrcture.Message;

namespace R2.Accounting.Test {

    [TestClass]
    public class LedgerTest : BaseTest {

        [TestMethod]
        public async Task CreateLedgerTest () {
            var user = IdentityService.Login (0);
            var parentCategory = new CreateLedger {
                Name = "Ledger Title",
                CreatorUser = new System.Guid (user.Id)
            };
            var result = await PostMessage<CreateLedger, CreateLedgerResult> (parentCategory);
            Assert.IsNotNull (result.Entity.Id);
            Assert.IsNotNull (result.Entity.CreationTime);
            Assert.AreEqual (result.Entity.CreatorUser, user.NickName);
            Assert.IsTrue (result.Succeeded);
        }

        [TestMethod]
        public async Task GetLedgerTest () {
            var user = IdentityService.Login (0);
            var ledger = new CreateLedger {
                Name = "Ledger Title",
                CreatorUser = new System.Guid (user.Id)
            };
            var CreateLedgerResult = await PostMessage<CreateLedger, CreateLedgerResult> (ledger);

            var getLedgerQuery = new GetLedgerQuery {
                Id = CreateLedgerResult.Entity.Id
            };
            var GetLedgerQueryResult = await PostMessage<GetLedgerQuery, GetLedgerQueryResult> (getLedgerQuery);
            Assert.IsNotNull (GetLedgerQueryResult.Entity);
            Assert.AreEqual (CreateLedgerResult.Entity.Id, GetLedgerQueryResult.Entity.Id);
            Assert.IsTrue (CreateLedgerResult.Succeeded);
        }

        [TestMethod]
        public async Task GetLedgerListTest () {
            var user = IdentityService.Login (0);
            var ledger = new CreateLedger {
                Name = "Ledger Title",
                CreatorUser = new System.Guid (user.Id)
            };
            await PostMessage<CreateLedger, CreateLedgerResult> (ledger);
            await PostMessage<CreateLedger, CreateLedgerResult> (ledger);

            // var getLedgerQuery = new GetLedgerListQuery {
            //     Take = 1
            // };
            // var GetLedgerQueryResult = await PostMessage<GetLedgerListQuery, GetLedgerListQueryResult> (getLedgerQuery);
            // Assert.AreEqual (GetLedgerQueryResult.Entities.Count, 1);
            // Assert.AreEqual (GetLedgerQueryResult.TotalCount, 2);
            // Assert.IsTrue (GetLedgerQueryResult.Succeeded);
        }

        [TestMethod]
        public async Task UpdateLedgerTest () {
            var user = IdentityService.Login (0);
            var ledger = new CreateLedger {
                Name = "Ledger Title",
                CreatorUser = new System.Guid (user.Id)
            };
            var createLedgerResult = await PostMessage<CreateLedger, CreateLedgerResult> (ledger);

            var updateLedger = new UpdateLedger {
                Id = createLedgerResult.Entity.Id,
                Name = "Ledger Title changed",
                ModifierUser = new System.Guid (user.Id)
            };
            var updateLedgerResult = await PostMessage<UpdateLedger, UpdateLedgerResult> (updateLedger);

            var getLedgerQuery = new GetLedgerQuery {
                Id = createLedgerResult.Entity.Id
            };
            var getLedgerQueryResult = await PostMessage<GetLedgerQuery, GetLedgerQueryResult> (getLedgerQuery);

            Assert.IsTrue (createLedgerResult.Succeeded);
            Assert.IsTrue (updateLedgerResult.Succeeded);
            Assert.IsTrue (getLedgerQueryResult.Succeeded);

            Assert.IsNotNull (getLedgerQueryResult.Entity);
            Assert.IsNotNull (getLedgerQueryResult.Entity.CreationTime);
            Assert.IsNotNull (getLedgerQueryResult.Entity.LastModificationTime);
            Assert.AreEqual (getLedgerQueryResult.Entity.Name, updateLedger.Name);
        }

        [TestMethod]
        public async Task DeleteLedgerTest () {
            var user = IdentityService.Login (0);
            var ledger = new CreateLedger {
                Name = "Ledger Title",
                CreatorUser = new System.Guid (user.Id)
            };
            var createLedgerResult = await PostMessage<CreateLedger, CreateLedgerResult> (ledger);
            await PostMessage<CreateLedger, CreateLedgerResult> (ledger);

            var deleteLedger = new DeleteLedger {
                Id = createLedgerResult.Entity.Id,
                DeleterUser = new System.Guid (user.Id)
            };
            var deleteLedgerResult = await PostMessage<DeleteLedger, Result> (deleteLedger);

            var getLedgerQuery = new GetLedgerQuery {
                Id = createLedgerResult.Entity.Id
            };
            var getLedgerQueryResult = await PostMessage<GetLedgerQuery, GetLedgerQueryResult> (getLedgerQuery);
            // var getLedgerListQueryResult = await PostMessage<GetLedgerListQuery, GetLedgerListQueryResult> (new GetLedgerListQuery ());

            Assert.IsTrue (createLedgerResult.Succeeded);
            Assert.IsTrue (deleteLedgerResult.Succeeded);
            Assert.IsTrue (getLedgerQueryResult.Succeeded);
            // Assert.IsTrue (getLedgerListQueryResult.Succeeded);

            Assert.IsNull (getLedgerQueryResult.Entity);
            // Assert.AreEqual (getLedgerListQueryResult.TotalCount, 1);
        }

    }
}