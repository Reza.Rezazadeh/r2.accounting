using System;
using System.Collections.Generic;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using R2.Accounting.Commands;
using R2.Accounting.Domain.Repository;
using R2.Accounting.Handlers;
using R2.Accounting.Helpers;
using R2.Framework.Domain.Context;
using R2.Framework.Domain.Repository;
using R2.Framework.Infrastrcture.Bus;
using R2.Framework.Infrastrcture.Message;
using R2.Framework.MB.Configurations;
using R2.Framework.MB.Extensions;
using R2.Framework.MB.Interfaces;

namespace R2.Accounting.Test {

    [TestClass]
    public class BaseTest {
        public ServiceCollection ServiceProvider { get; private set; }

        [TestInitialize]
        public void InitTest () {
            ServiceProvider = new ServiceCollection ();
            Assembly.Load ("R2.Accounting");
            Assembly.Load ("R2.Accounting.Handlers");
            Assembly.Load ("R2.Accounting.QueryHandlers");
            ServiceProvider.AddHandles ();
            InitDataBase (ServiceProvider);
        }

        private void InitDataBase (ServiceCollection services) {
            var dataContextBuilder = new DbContextOptionsBuilder<ShopNetDataContext> ()
                .UseSqlite (CreateInMemoryDatabase ());
            var context = new ShopNetDataContext (dataContextBuilder.Options);
            services.AddDbContext<ShopNetDataContext> (options => options.UseSqlite (CreateInMemoryDatabase ()));
            services.AddSingleton (context);
            services.AddSingleton<IUnitOfWork, UnitOfWork<ShopNetDataContext>> ();
            services.AddScoped (typeof (IRepository<>), typeof (Repository<>));
            services.AddScoped (typeof (IUserRepository), _ => new UserRepository (context));
            context.Database.EnsureCreated ();
        }

        private DbConnection CreateInMemoryDatabase () {
            // var connection = new SqliteConnection ("Filename=:memory:");
            var connection = new SqliteConnection ("Filename=accounting.test.db");
            connection.Open ();
            return connection;
        }

        public async Task<TOut> PostMessage<TIn, TOut> (TIn message)
        where TIn : class, ICommand, new ()
        where TOut : class, IResult, new () {
            try {
                var provider = ServiceProvider.BuildServiceProvider ();
                var handler = provider.GetService<ICommandConsumer<TIn, TOut>> ();
                return await handler.Consume (message);
            } catch (Exception ex) {
                throw new Exception (ex.Message);
            }
        }
    }
}