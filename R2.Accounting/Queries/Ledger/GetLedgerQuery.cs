﻿using System;
using R2.Accounting.Dtos;
using R2.Framework.Infrastrcture.Message;

namespace R2.Accounting.Commands.Ledger {
    public class GetLedgerQuery : Request<GetLedgerQueryResult> {
        public Guid Id { get; set; }
    }

    public class GetLedgerQueryResult : SingleQueryResult<LedgerDto> { }

}