using System;
using System.Linq;
using FluentValidation;
using Microsoft.Extensions.DependencyInjection;

namespace R2.Accounting.Helpers
{
    public static class CommandPipelinesExtensions {
        public static ServiceCollection AddCommandsValidation (this ServiceCollection services) {
            AppDomain.CurrentDomain.GetAssemblies ().Where (c => !c.IsDynamic).ToList ()
                .ForEach (assembly => {
                    var validatorTypes = assembly
                        .GetExportedTypes ()
                        .Where (type => type.GetInterfaces ().Any (y => (typeof (IValidator) == y)));
                    if (validatorTypes != null) {
                        // var validators = validatorTypes.Select (x => Activator.CreateInstance (x));

                        foreach (var configuration in validatorTypes) {
                            services.AddTransient (configuration.GetInterfaces () [0], configuration);
                        }
                    }

                });

            return services;

        }

    }

}