﻿using System;

namespace R2.Accounting.Dtos
{
    public class LedgerDto
    {
        public string Name { get; set; }
        public string CreatorUser { get; set; }
        public DateTime CreationTime { get; set; }
        public string LastModifierUser { get; set; }
        public DateTime? LastModificationTime { get; set; }
        public Guid Id { get; set; }
    }
}