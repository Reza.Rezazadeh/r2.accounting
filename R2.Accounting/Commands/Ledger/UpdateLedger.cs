using System;
using R2.Accounting.Dtos;
using R2.Framework.Infrastrcture.Message;

namespace R2.Accounting.Commands.Ledger
{

    public class UpdateLedger : Request<UpdateLedgerResult> {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Guid ModifierUser { get; set; }
    }

    public class UpdateLedgerResult : Result {
        public LedgerDto Entity { get; set; }
    }

}