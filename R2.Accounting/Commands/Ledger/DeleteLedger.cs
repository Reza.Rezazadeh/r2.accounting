using System;
using R2.Framework.Infrastrcture.Message;

namespace R2.Accounting.Commands.Ledger
{
    public class DeleteLedger : Request<Result> {
        public Guid Id { get; set; }
        public Guid DeleterUser { get; set; }
    }

}