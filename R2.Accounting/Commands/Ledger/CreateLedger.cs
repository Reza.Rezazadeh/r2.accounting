using System;
using R2.Accounting.Dtos;
using R2.Framework.Infrastrcture.Message;

namespace R2.Accounting.Commands.Ledger
{
    public class CreateLedger : Request<CreateLedgerResult> {
        public string Name { get; set; }
        public Guid CreatorUser { get; set; }
    }

    public class CreateLedgerResult : Result {
        public LedgerDto Entity { get; set; }
    }

}