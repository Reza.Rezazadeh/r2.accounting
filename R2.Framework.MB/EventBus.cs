using System.Threading.Tasks;
using MassTransit;
using R2.Framework.Infrastrcture.Bus;
using R2.Framework.Infrastrcture.Message;

namespace R2.Framework.MB
{

    public class EventBus : IEventBus {
        private readonly IPublishEndpoint _publishEndpoint;

        public EventBus (IPublishEndpoint publishEndpoint) {
            _publishEndpoint = publishEndpoint;
        }

        public async Task Publish<TEvent> (TEvent eventDto)
        where TEvent : class, IEvent {
            await _publishEndpoint.Publish (eventDto);
        }
    }

}