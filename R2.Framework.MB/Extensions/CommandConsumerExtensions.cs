﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using R2.Framework.Infrastrcture.Message;
using R2.Framework.MB.Configurations;
using R2.Framework.MB.Factory;
using R2.Framework.MB.Interfaces;

namespace R2.Framework.MB.Extensions {

    public static class CommandConsumerExtensions {
        public static ServiceCollection RegisterCommandConsumer (
            this ServiceCollection serviceProvider,
            Action<CommandTypesOptions> commandOptions,
            RabbitMqOptions optionsRetriever) {
            var integrationCommandOptions = new CommandTypesOptions ();
            commandOptions?.Invoke (integrationCommandOptions);
            var commandsTypes = integrationCommandOptions.CommandTypes;

            foreach (var types in commandsTypes) {
                var typeArguments =
                    new [] {
                        types.Key,
                        types.Value
                    };

                var adapterTypeWithTypeArguments = typeof (CommandConsumerAdapter<,>).MakeGenericType (typeArguments);

                serviceProvider.AddSingleton (adapterTypeWithTypeArguments);
            }

            serviceProvider.AddSingleton<ICommandConsumerMessageBusControl> (context => {
                return BusControlWrapperFactory.CreateForCommandConsumer (context, optionsRetriever, commandsTypes);
            });

            serviceProvider.AddSingleton<ICommandConsumerMessageBusManager, MessageBusManager<ICommandConsumerMessageBusControl>> ();

            return serviceProvider;
        }
}

}