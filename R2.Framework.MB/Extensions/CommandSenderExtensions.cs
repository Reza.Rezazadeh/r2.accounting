using System;
using Microsoft.Extensions.DependencyInjection;
using R2.Framework.Infrastrcture.Bus;
using R2.Framework.MB.Configurations;
using R2.Framework.MB.Factory;
using R2.Framework.MB.Interfaces;

namespace R2.Framework.MB.Extensions
{

    public static class CommandSenderExtensions {
        public static IServiceCollection RegisterCommandSender (
            this IServiceCollection serviceProvider,
            Action<CommandTypesOptions> commandOptions,
            RabbitMqOptions optionsRetriever) {
            var commandsOptions = new CommandTypesOptions ();
            commandOptions?.Invoke (commandsOptions);

            serviceProvider.AddSingleton<ICommandSenderMessageBusControl> (_ => {
                return BusControlWrapperFactory.CreateForCommandSender (optionsRetriever, commandsOptions.CommandTypes);
            });

            serviceProvider.AddSingleton<ICommandSenderMessageBusManager, MessageBusManager<ICommandSenderMessageBusControl>> ();
            serviceProvider.AddSingleton<ICommandBus, CommandBus> ();

            return serviceProvider;
        }
    }

}