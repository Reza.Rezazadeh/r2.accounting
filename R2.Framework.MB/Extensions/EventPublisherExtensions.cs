using MassTransit;
using Microsoft.Extensions.DependencyInjection;
using R2.Framework.Infrastrcture.Bus;
using R2.Framework.MB.Configurations;
using R2.Framework.MB.Factory;

namespace R2.Framework.MB.Extensions
{
    public static class EventPublisherExtensions
    {
        public static ServiceCollection RegisterEventPublisher(
            this ServiceCollection serviceProvider,
            RabbitMqOptions optionsRetriever)
        {
            serviceProvider.AddSingleton<IPublishEndpoint> (_ => {
                return BusControlWrapperFactory.CreateForEventPublisher(optionsRetriever);
            });
            
            serviceProvider.AddSingleton<IEventBus,EventBus>();

            return serviceProvider;
        }
    }

}