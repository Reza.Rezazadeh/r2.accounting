using System;
using Microsoft.Extensions.DependencyInjection;
using R2.Framework.MB.Configurations;
using R2.Framework.MB.Factory;
using R2.Framework.MB.Interfaces;

namespace R2.Framework.MB.Extensions
{

    public static class EventConsumerExtensions {
        public static ServiceCollection RegisterEventConsumer (
            this ServiceCollection serviceProvider,
            Action<EventTypesOptions> eventOptions,
            RabbitMqOptions optionsRetriever) {
            var eventsOptions = new EventTypesOptions ();
            eventOptions?.Invoke (eventsOptions);
            var eventsTypes = eventsOptions.EventTypes;

            foreach (var eventType in eventsTypes) {
                var adapterType = typeof (EventConsumerAdapter<>).MakeGenericType (eventType);

                serviceProvider.AddSingleton (adapterType);
            }

            serviceProvider.AddSingleton<IEventConsumerMessageBusControl> (context => {
                return BusControlWrapperFactory.CreateForEventConsumer (context, optionsRetriever, eventsOptions.EventTypes);
            });

            serviceProvider.AddSingleton<IEventConsumerMessageBusManager, MessageBusManager<IEventConsumerMessageBusControl>> ();

            return serviceProvider;
        }
    }

}