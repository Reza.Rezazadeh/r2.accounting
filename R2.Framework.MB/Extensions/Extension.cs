using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Microsoft.Extensions.DependencyInjection;
using R2.Framework.Infrastrcture.Bus;
using R2.Framework.Infrastrcture.Decorators;
using R2.Framework.Infrastrcture.Message;
using R2.Framework.MB.Configurations;

namespace R2.Framework.MB.Extensions {
    public static class TypeExtension {

        public static ServiceCollection AddTestHandles (this ServiceCollection service) {

            var commandTypes = GetEnumerableOfType (typeof (Request<>));
            var handlerTypes = GetEnumerableOfType (typeof (MessageHandler<,>));

            foreach (var item in commandTypes) {
                var handler = handlerTypes.FirstOrDefault (c => c.GetInterfaces ().FirstOrDefault ().GenericTypeArguments[0] == item);
                if (handler != null)
                    service.AddTransient (item.GetInterfaces ().FirstOrDefault (), handler);
                // AddHandler (service, item);
            }
            return service;
        }

        public static IServiceCollection AddHandles (this IServiceCollection service) {

            List<Type> handlerTypes = AppDomain.CurrentDomain.GetAssemblies ()
                .SelectMany (assembly => assembly.GetTypes ())
                .Where (x => x.GetInterfaces ().Any (y => IsHandlerInterface (y)))
                .Where (x => x.Name.EndsWith ("Handler"))
                .ToList ();

            foreach (var item in handlerTypes) {
                service.AddTransient (item.GetInterfaces ().FirstOrDefault (), item);
                // AddHandler (service, item);
            }
            return service;
        }

        public static ServiceCollection AddHandlers (this ServiceCollection service) {

            List<Type> handlerTypes = AppDomain.CurrentDomain.GetAssemblies ()
                .SelectMany (assembly => assembly.GetTypes ())
                .Where (x => x.GetInterfaces ().Any (y => IsHandlerInterface (y)))
                .Where (x => x.Name.EndsWith ("Handler"))
                .ToList ();

            foreach (var item in handlerTypes) {
                service.AddTransient (item.GetInterfaces ().FirstOrDefault (), item);
                // AddHandler (service, item);
            }
            return service;
        }

        public static ServiceCollection AddCommandConsumers (this ServiceCollection service, RabbitMqOptions rabbitMqOptions) {
            var commandsTypes = GetEnumerableOfType (typeof (Request<>));
            service.RegisterCommandConsumer (
                commandOptions => {
                    foreach (var item in commandsTypes) {
                        commandOptions.ConsumesCommand (item, item.GetInterfaces ().FirstOrDefault ().GetGenericArguments ().FirstOrDefault ());
                    }
                }, rabbitMqOptions
            );

            return service;
        }
        public static bool IsInheritedFrom (this Type type, Type Lookup) {
            var baseType = type.BaseType;
            if (baseType == null)
                return false;

            if (baseType.IsGenericType &&
                baseType.GetGenericTypeDefinition () == Lookup)
                return true;

            return baseType.IsInheritedFrom (Lookup);
        }

        public static IEnumerable<Type> GetEnumerableOfType (Type type) {
            return AppDomain.CurrentDomain.GetAssemblies ()
                .SelectMany (assembly => assembly.GetTypes ())
                .Where (x => x.IsClass && !x.IsAbstract && x.IsInheritedFrom (type))
                .Select (c => c)
                .ToList ();
        }

        // private static void AddHandler (ServiceCollection services, Type type) {
        //     object[] attributes = type.GetCustomAttributes (false);

        //     List<Type> pipeline = attributes
        //         .Select (x => ToDecorator (x))
        //         .Concat (new [] { type })
        //         .Reverse ()
        //         .ToList ();

        //     Type interfaceType = type.GetInterfaces ().Single (y => IsHandlerInterface (y));
        //     Func<IServiceProvider, object> factory = BuildPipeline (pipeline, type);

        //     // services.AddTransient (interfaceType, factory);
        //     services.AddTransient (type.GetInterfaces ().FirstOrDefault (), factory);

        // }

        // private static Func<IServiceProvider, object> BuildPipeline (List<Type> pipeline, Type interfaceType) {
        //     List<ConstructorInfo> ctors = pipeline
        //         .Select (x => {
        //             Type type = x.IsGenericType ? x.MakeGenericType (interfaceType.GenericTypeArguments) : x;
        //             return type.GetConstructors ().Single ();
        //         })
        //         .ToList ();

        //     Func<IServiceProvider, object> func = provider => {
        //         object current = null;

        //         foreach (ConstructorInfo ctor in ctors) {
        //             List<ParameterInfo> parameterInfos = ctor.GetParameters ().ToList ();

        //             object[] parameters = GetParameters (parameterInfos, current, provider);

        //             current = ctor.Invoke (parameters);
        //         }

        //         return current;
        //     };

        //     return func;
        // }

        // private static object[] GetParameters (List<ParameterInfo> parameterInfos, object current, IServiceProvider provider) {
        //     var result = new object[parameterInfos.Count];

        //     for (int i = 0; i < parameterInfos.Count; i++) {
        //         result[i] = GetParameter (parameterInfos[i], current, provider);
        //     }

        //     return result;
        // }

        // private static object GetParameter (ParameterInfo parameterInfo, object current, IServiceProvider provider) {
        //     Type parameterType = parameterInfo.ParameterType;

        //     if (IsHandlerInterface (parameterType))
        //         return current;

        //     object service = provider.GetService (parameterType);
        //     if (service != null)
        //         return service;

        //     throw new ArgumentException ($"Type {parameterType} not found");
        // }

        // private static Type ToDecorator (object attribute) {
        //     Type type = attribute.GetType ();

        //     if (type == typeof (AuditLogAttribute))
        //         return typeof (AuditLoggingDecorator<,>);

        //     // other attributes go here

        //     throw new ArgumentException (attribute.ToString ());
        // }

        private static bool IsHandlerInterface (Type type) {
            if (!type.IsGenericType)
                return false;

            Type typeDefinition = type.GetGenericTypeDefinition ();

            return typeDefinition == typeof (ICommandConsumer<,>);
        }
    }
}