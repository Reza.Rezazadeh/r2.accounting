﻿using System;
using System.Threading.Tasks;
using MassTransit;
using R2.Framework.Infrastrcture.Bus;
using R2.Framework.Infrastrcture.Message;
using R2.Framework.MB.Interfaces;

namespace R2.Framework.MB
{

    public sealed class CommandBus : ICommandBus {
        private readonly ICommandSenderMessageBusControl _busControl;

        public CommandBus (ICommandSenderMessageBusControl busControl) {
            _busControl = busControl ??
                throw new ArgumentNullException (nameof (busControl));
        }

        public async Task<TCommandResult> Send<TCommand, TCommandResult> (TCommand command)
        where TCommand : class, ICommand
        where TCommandResult : class, IMessage {
            var timeout = RequestTimeout.After (s: 30);
            var requestClient = _busControl.CreateRequestClient<TCommand> (timeout);
            var requestHandler = requestClient.Create (command);
            var response = await requestHandler.GetResponse<TCommandResult> (true);
            var result = response.Message;

            return result;
        }

    }

}