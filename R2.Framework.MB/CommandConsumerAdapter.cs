using System;
using System.Threading.Tasks;
using MassTransit;
using R2.Framework.Infrastrcture.Bus;
using R2.Framework.Infrastrcture.Message;

namespace R2.Framework.MB {
    public sealed class CommandConsumerAdapter<TCommand, TCommandResponse>
        : IConsumer<TCommand>
        where TCommand : class, ICommand
    where TCommandResponse : class, IMessage {
        private readonly ICommandConsumer<TCommand, TCommandResponse> _commandConsumer;

        public CommandConsumerAdapter (ICommandConsumer<TCommand, TCommandResponse> commandConsumer) {
            _commandConsumer = commandConsumer ??
                throw new ArgumentNullException (nameof (commandConsumer));
        }

        public async Task Consume (ConsumeContext<TCommand> context) {
            try {
                var response = await _commandConsumer.Consume (context.Message);

                await context.RespondAsync (response);
            } catch (System.Exception ex) {
                await Console.Out.WriteLineAsync (ex.Message);
                throw;
            }

        }
    }

}