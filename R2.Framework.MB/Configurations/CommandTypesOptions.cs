using System;
using System.Collections.Generic;
using R2.Framework.Infrastrcture.Message;

namespace R2.Framework.MB.Configurations {

    public sealed class CommandTypesOptions {
        private readonly Dictionary<Type, Type> _commandsTypes;
        public IReadOnlyDictionary<Type, Type> CommandTypes => _commandsTypes;

        public CommandTypesOptions () {
            _commandsTypes = new Dictionary<Type, Type> ();
        }

        public CommandTypesOptions ConsumesCommand<TCommandType, TResponseType> ()
        where TCommandType : ICommand
        where TResponseType : IMessage {
            return AddCommand<TCommandType, TResponseType> ();
        }

        public CommandTypesOptions ConsumesCommand (Type commandType, Type responseType) {
            return AddCommand (commandType, responseType);
        }

        public CommandTypesOptions SendsCommand<TCommandType, TResponseType> ()
        where TCommandType : ICommand
        where TResponseType : IMessage {
            return AddCommand<TCommandType, TResponseType> ();
        }

        public CommandTypesOptions SendsCommand (Type commandType, Type responseType) {
            return AddCommand (commandType, responseType);
        }

        private CommandTypesOptions AddCommand<TCommandType, TResponseType> ()
        where TCommandType : ICommand
        where TResponseType : IMessage {
            var commandType = typeof (TCommandType);
            var responseType = typeof (TResponseType);

            _commandsTypes.Add (commandType, responseType);

            return this;
        }

        public CommandTypesOptions AddCommand (Type commandType, Type responseType) {
            _commandsTypes.Add (commandType, responseType);

            return this;
        }
    }

}