namespace R2.Framework.MB.Configurations
{
    public class RetryPolicyOptions
    {
        public int RetryAttempts { get; set; }
        public int RetryIntervalMilliseconds { get; set; }
    }

}