using System;
using System.Collections.Generic;
using MassTransit;
using MassTransit.RabbitMqTransport;
using R2.Framework.MB.Configurations;

namespace R2.Framework.MB.Factory
{
    public static class BusControlWrapperFactory {
        public static IBusControlWrapper CreateForCommandConsumer (
            IServiceProvider serviceProvider,
            RabbitMqOptions messageBusOptions,
            IReadOnlyDictionary<Type, Type> commandsTypes) {
            var queueName = messageBusOptions.EndpointName;

            var busControl = Bus.Factory.CreateUsingRabbitMq (cfg => {
                CreateRabbitMqHost (cfg, messageBusOptions);

                cfg.ReceiveEndpoint (queueName, consumer => {
                    foreach (var types in commandsTypes) {
                    var typeArguments =
                    new [] {
                    types.Key,
                    types.Value
                            };
                        var adapterTypeWithTypeArguments = typeof (CommandConsumerAdapter<,>).MakeGenericType (typeArguments);
                        var resolvedType = serviceProvider.GetService (adapterTypeWithTypeArguments);

                        consumer.Consumer (adapterTypeWithTypeArguments, type => resolvedType);
                    }
                });
            });

            var busControlWrapper = new BusControlWrapper (busControl);
            return busControlWrapper;
        }

        public static IBusControlWrapper CreateForCommandSender (
            RabbitMqOptions messageBusOptions,
            IReadOnlyDictionary<Type, Type> commandTypes) {

            var busControl = Bus.Factory.CreateUsingRabbitMq (cfg => {
                CreateRabbitMqHost (cfg, messageBusOptions);
            });

            var uriType = typeof (Uri);
            var parametersTypes =
                new [] {
                    uriType.MakeByRefType ()
                };
            var queueName = messageBusOptions.EndpointName;
            var serverAddress = busControl.Address;
            var uri = new Uri (serverAddress, queueName);

            var endPointConventionTryGetDestinationAddress = typeof (EndpointConvention).GetMethod ("TryGetDestinationAddress", parametersTypes);
            foreach (var commandType in commandTypes.Keys) {
                var endPointConventionTryGetDestinationAddressGeneric = endPointConventionTryGetDestinationAddress?.MakeGenericMethod (commandType);
                var tryGetDestinationAddressParameters =
                    new object[] {
                        uri
                    };

                endPointConventionTryGetDestinationAddressGeneric?.Invoke (null, tryGetDestinationAddressParameters);
            }

            var busControlWrapper = new BusControlWrapper (busControl);
            return busControlWrapper;
        }

        public static IBusControlWrapper CreateForEventConsumer (
            IServiceProvider serviceProvider,
            RabbitMqOptions messageBusOptions,
            IReadOnlyList<Type> eventsTypes) {

            var queueName = messageBusOptions.EndpointName;
            var retryPolicyOptions = messageBusOptions.RetryPolicyOptions;
            var busControl = Bus.Factory.CreateUsingRabbitMq (cfg => {
                CreateRabbitMqHost (cfg, messageBusOptions);

                cfg.ReceiveEndpoint (queueName, consumer => {
                    ConfigureRetryPolicy (consumer, retryPolicyOptions);

                    foreach (var eventDtoType in eventsTypes) {
                        var adapterType = typeof (EventConsumerAdapter<>).MakeGenericType (eventDtoType);
                        var resolvedType = serviceProvider.GetService (adapterType);

                        consumer.Consumer (adapterType, type => resolvedType);
                    }
                });
            });

            var busControlWrapper = new BusControlWrapper (busControl);
            return busControlWrapper;
        }

        public static IBusControlWrapper CreateForEventPublisher (
            RabbitMqOptions messageBusOptions) {
            var busControl = Bus.Factory.CreateUsingRabbitMq (cfg => {
                CreateRabbitMqHost (cfg, messageBusOptions);
            });

            var busControlWrapper = new BusControlWrapper (busControl);
            return busControlWrapper;
        }

        private static void CreateRabbitMqHost (IRabbitMqBusFactoryConfigurator configurator, RabbitMqOptions options) {
            var hostName = options.HostName;
            var port = options.Port;
            var username = options.Username;
            var password = options.Password;
            var useSsl = options.UseSsl;
            var autoDelete = options.AutoDelete;

            configurator.AutoDelete = autoDelete;

            configurator.Host (hostName, h => {
                h.Username (username);
                h.Password (password);
            });
        }

        private static void ConfigureRetryPolicy (IRabbitMqReceiveEndpointConfigurator consumerConfigurator, RetryPolicyOptions retryPolicyOptions) {
            // if (retryPolicyOptions != null) {
            //     var attempts = retryPolicyOptions.RetryAttempts;
            //     var interval = retryPolicyOptions.RetryIntervalMilliseconds;
            //     consumerConfigurator.UseMessageRetry (retryConfiguration => {
            //          retryConfiguration.Interval (attempts, interval);
            //     });
            //     consumerConfigurator.UseInMemoryOutbox ();
            // }
        }
    }

}