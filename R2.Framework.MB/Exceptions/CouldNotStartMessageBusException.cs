﻿using System;

namespace R2.Framework.MB.Exceptions
{
    public class CouldNotStartMessageBusException
        : Exception
    {
        public CouldNotStartMessageBusException(string message)
            : base(message)
        {
        }

        public CouldNotStartMessageBusException(string message, Exception exception)
            : base(message, exception)
        {
        }
    }

}