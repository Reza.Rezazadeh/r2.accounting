using System.Threading.Tasks;

namespace R2.Framework.MB.Interfaces
{
    public interface IMessageBusManager {
        Task Start (int maxMillisecondsWait = 50000);
        Task Stop ();
    }

}