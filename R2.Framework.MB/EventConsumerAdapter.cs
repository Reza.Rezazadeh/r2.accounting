using System.Threading.Tasks;
using MassTransit;
using R2.Framework.Infrastrcture.Bus;
using R2.Framework.Infrastrcture.Message;

namespace R2.Framework.MB
{
    public class EventConsumerAdapter<TEvent>
        : IConsumer<TEvent>
            where TEvent : class, IEvent
    {
        private readonly IEventConsumer<TEvent> _eventConsumer;

        public EventConsumerAdapter(IEventConsumer<TEvent> eventConsumer)
        {
            _eventConsumer = eventConsumer;
        }

        public async Task Consume(ConsumeContext<TEvent> context)
        {
            var message = context.Message;
            await _eventConsumer.Consume(message);
        }
    }

}