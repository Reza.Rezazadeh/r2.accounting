using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace R2.Accounting.Web.Auth
{
    public class PhoneLoginViewModel
    {
        [Required]
        [DataType(DataType.PhoneNumber)]
        [JsonProperty("phone")]
        public string PhoneNumber { get; set; }
    }
}