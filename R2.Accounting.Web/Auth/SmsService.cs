using System;
using System.Threading.Tasks;

namespace R2.Accounting.Web.Auth {
    public interface ISmsService {
        Task<bool> SendAsync (string phoneNumber, string body);
    }
    public class SmsService : ISmsService {
        public Task<bool> SendAsync (string phoneNumber, string body) {
            Console.WriteLine($"{phoneNumber}: {body}");
            return Task.FromResult (true);
        }
    }
}