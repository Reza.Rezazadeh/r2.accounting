﻿using System.Reflection;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.SqlServer;
using Microsoft.Extensions.Configuration;
using R2.Accounting.Domain;
namespace R2.Accounting.Web.Auth {
    public class AuthDbContext : IdentityDbContext<User> {
        public AuthDbContext (DbContextOptions<AuthDbContext> options) : base (options) { }

        protected override void OnModelCreating (ModelBuilder builder) {
            base.OnModelCreating (builder);
        }
    }

    public class AuthDbContextFactory : IDesignTimeDbContextFactory<AuthDbContext> {
        public AuthDbContext CreateDbContext (string[] args) {
            var migrationsAssembly = typeof (Startup).GetTypeInfo ().Assembly.GetName ().Name;
            var connectionString = "Password=123@.com;Persist Security Info=True;User ID=sa;Initial Catalog=R2.Accounting;Data Source=.\\sqlexpress";

            var optionsBuilder = new DbContextOptionsBuilder<AuthDbContext> ();
            optionsBuilder.UseSqlServer (connectionString, sql => sql.MigrationsAssembly (migrationsAssembly));

            return new AuthDbContext (optionsBuilder.Options);
        }
    }
}