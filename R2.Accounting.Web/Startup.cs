using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using R2.Accounting.Commands.Ledger;
using R2.Accounting.Domain;
using R2.Accounting.Web.Auth;
using R2.Framework.Infrastrcture.Bus;
using R2.Framework.Infrastrcture.Message;
using R2.Framework.MB.Configurations;
using R2.Framework.MB.Extensions;
using R2.Framework.MB.Interfaces;

namespace R2.Accounting.Web {
    public class Startup {
        public Startup (IConfiguration configuration) {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices (IServiceCollection services) {
            services.AddSingleton<IServiceCollection, ServiceCollection> ();

            var migrationsAssembly = typeof (Startup).GetTypeInfo ().Assembly.GetName ().Name;
            var connectionString = Configuration.GetConnectionString ("AuthConnection");

            services.AddDbContext<AuthDbContext> (options => {
                options.UseSqlServer (connectionString,
                    sql => sql.MigrationsAssembly (migrationsAssembly));
            });

            services.AddIdentity<User, IdentityRole> (opt => {
                    opt.Password.RequireDigit = false;
                    opt.Password.RequireLowercase = false;
                    opt.Password.RequireUppercase = false;
                    opt.Password.RequireNonAlphanumeric = false;
                    opt.Password.RequiredLength = 6;
                })
                .AddEntityFrameworkStores<AuthDbContext> ()
                .AddDefaultTokenProviders ();

            services.AddIdentityServer (options => {
                    options.Events.RaiseErrorEvents = true;
                    options.Events.RaiseFailureEvents = true;
                })
                .AddExtensionGrantValidator<PhoneNumberTokenGrantValidator> ()
                .AddDeveloperSigningCredential ()
                .AddInMemoryApiResources (AuthConfig.GetApiResources ())
                .AddInMemoryIdentityResources (AuthConfig.GetIdentityResources ())
                .AddInMemoryClients (AuthConfig.GetClients ())
                .AddAspNetIdentity<User> ();

            services.AddSingleton<ISmsService, SmsService> ();
            services.AddControllers ();

            InitDataBus (services).GetAwaiter ().GetResult ();
            services.AddMvc ();
            services.AddSwaggerGen (c => {
                c.SwaggerDoc ("v1", new OpenApiInfo { Title = "Accounting API", Version = "v1" });
            });
        }

        private async Task InitDataBus (IServiceCollection services) {
            // var rabbitMqOptions = new RabbitMqOptions ();
            // Configuration.GetSection (RabbitMqOptions.Position).Bind (rabbitMqOptions);
            // services.RegisterCommandSender (null, rabbitMqOptions);
            // var container = services.BuildServiceProvider ();

            // var _commandSender = container.GetService<ICommandSenderMessageBusManager> ();
            // await _commandSender.Start ();
            var rabbitMqOptions = new RabbitMqOptions {
                HostName = "rabbitmq://localhost",
                Username = "guest",
                Password = "guest",
                EndpointName = "R2.Bus.Web",
                AutoDelete = true
            };

            services.RegisterCommandSender (null, rabbitMqOptions);

            var container = services.BuildServiceProvider ();

            var _commandSender = container.GetService<ICommandSenderMessageBusManager> ();
            var bus = container.GetService<ICommandBus> ();
            services.AddSingleton (bus);
            await _commandSender.Start ();

        }

        public void Configure (IApplicationBuilder app, IWebHostEnvironment env) {
            if (env.IsDevelopment ()) {
                app.UseDeveloperExceptionPage ();
            }
            app.UseSwagger ();
            app.UseSwaggerUI (c => {
                c.SwaggerEndpoint ("/swagger/v1/swagger.json", "Accounting API V1");
            });
            app.UseHttpsRedirection ();

            app.UseRouting ();

            app.UseIdentityServer ();

            app.UseEndpoints (endpoints => {
                endpoints.MapDefaultControllerRoute ();
            });
        }
    }
}