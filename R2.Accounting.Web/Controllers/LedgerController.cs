using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using R2.Accounting.Commands.Ledger;
using R2.Framework.Infrastrcture.Bus;
namespace R2.Accounting.Web.Controllers {
    [Route ("api/[controller]")]
    public class LedgerController : BaseController {

        [HttpPost]
        [Route ("CreateLedger")]
        public async Task<CreateLedgerResult> CreateLedger (CreateLedger ledger) {
            var result = await PostMessage<CreateLedger, CreateLedgerResult> (ledger);
            return result;
        }
    }
}