using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using R2.Accounting.Commands.Ledger;
using R2.Framework.Infrastrcture.Bus;
using R2.Framework.Infrastrcture.Message;

namespace R2.Accounting.Web {
    public class BaseController : ControllerBase {
        public async Task<TOut> PostMessage<TIn, TOut> (TIn message)
        where TIn : class, ICommand, new ()
        where TOut : class, IResult, new () {
            var Bus = HttpContext.RequestServices.GetService<ICommandBus> ();
            return await Bus.Send<TIn, TOut> (message);
        }

    }
}