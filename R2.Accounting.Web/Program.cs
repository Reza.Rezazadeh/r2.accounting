using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace R2.Accounting.Web {
    public class Program {
        public static void Main (string[] args) {
            var host = CreateHostBuilder (args).Build ();
            var config = host.Services.GetRequiredService<IConfiguration> ();
            var connectionString = config.GetConnectionString ("AuthConnection");
            SeedData.EnsureSeedData (connectionString);

            host.Run ();
        }

        public static IHostBuilder CreateHostBuilder (string[] args) =>
            Host.CreateDefaultBuilder (args)
            .ConfigureWebHostDefaults (webBuilder => {
                webBuilder.UseUrls ("https://localhost:5000");
                webBuilder.UseStartup<Startup> ();
            });
    }
}