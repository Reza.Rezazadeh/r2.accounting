﻿using System;
using System.Linq;
using System.Threading.Tasks;
using R2.Accounting.Commands;
using R2.Accounting.Commands.Ledger;
using R2.Accounting.Domain;
using R2.Accounting.Domain.Repository;
using R2.Accounting.Handlers.Mappers;
using R2.Framework.Domain.Context;
using R2.Framework.Infrastrcture.Bus;

namespace R2.Accounting.Handlers {

    public class CreateLedgerHandler : MessageHandler<CreateLedger, CreateLedgerResult> {
        private readonly IUnitOfWork unitOfWork;
        private readonly IUserRepository userRepository;

        public CreateLedgerHandler (IUnitOfWork unitOfWork, IUserRepository userRepository) {
            this.unitOfWork = unitOfWork;
            this.userRepository = userRepository;
        }
        public override async Task<CreateLedgerResult> Consume (CreateLedger command) {
            var ledgerRepository = unitOfWork.GetRepository<Ledger> ();

             var user = await userRepository.GetUser(command.CreatorUser);
            var ledger = new Ledger { Name = command.Name, CreatorUser = user };
            ledger.AddPermission (user,LedgerPermission.Edit);
            await ledgerRepository.InsertAsync (ledger);
                await unitOfWork.SaveChangesAsync ();
                
            return new CreateLedgerResult {
                Entity = ledger.MapToDto ()
            };

        }
    }
}