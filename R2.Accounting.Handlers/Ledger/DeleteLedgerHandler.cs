﻿using System;
using System.Linq;
using System.Threading.Tasks;
using R2.Accounting.Commands;
using R2.Accounting.Commands.Ledger;
using R2.Accounting.Domain;
using R2.Accounting.Domain.Repository;
using R2.Accounting.Handlers.Mappers;
using R2.Framework.Domain.Context;
using R2.Framework.Infrastrcture.Bus;
using R2.Framework.Infrastrcture.Message;

namespace R2.Accounting.Handlers {

    public class DeleteLedgerHandler : MessageHandler<DeleteLedger, Result> {
        private readonly IUnitOfWork unitOfWork;
        private readonly IUserRepository userRepository;

        public DeleteLedgerHandler (IUnitOfWork unitOfWork, IUserRepository userRepository) {
            this.unitOfWork = unitOfWork;
            this.userRepository = userRepository;
        }
        public override async Task<Result> Consume (DeleteLedger command) {
            var ledgerRepository = unitOfWork.GetRepository<Ledger> ();

            var user = await userRepository.GetUser (command.DeleterUser);
            var ledger = unitOfWork.GetRepository<Ledger> ().Query ().FirstOrDefault (c => c.Id == command.Id);
            ledger.DeleterUser = user;
            ledgerRepository.Delete (ledger);
            await unitOfWork.SaveChangesAsync ();
            return new Result ();
        }
    }
}