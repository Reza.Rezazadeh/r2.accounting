﻿using System;
using System.Linq;
using System.Threading.Tasks;
using R2.Accounting.Commands;
using R2.Accounting.Commands.Ledger;
using R2.Accounting.Domain;
using R2.Accounting.Domain.Repository;
using R2.Accounting.Handlers.Mappers;
using R2.Framework.Domain.Context;
using R2.Framework.Infrastrcture.Bus;

namespace R2.Accounting.Handlers {

    public class UpdateLedgerHandler : MessageHandler<UpdateLedger, UpdateLedgerResult> {
        private readonly IUnitOfWork unitOfWork;
        private readonly IUserRepository userRepository;

        public UpdateLedgerHandler (IUnitOfWork unitOfWork, IUserRepository userRepository) {
            this.unitOfWork = unitOfWork;
            this.userRepository = userRepository;
        }
        public override async Task<UpdateLedgerResult> Consume (UpdateLedger command) {
            var ledgerRepository = unitOfWork.GetRepository<Ledger> ();
            var user = await userRepository.GetUser (command.ModifierUser);
            var ledger = unitOfWork.GetRepository<Ledger> ().Query ().FirstOrDefault (c => c.Id == command.Id);
            ledger.Name = command.Name;
            ledger.LastModifierUser = user;
            ledgerRepository.Update (ledger);
            await unitOfWork.SaveChangesAsync ();
            return new UpdateLedgerResult {
                Entity = ledger.MapToDto ()
            };

        }
    }
}