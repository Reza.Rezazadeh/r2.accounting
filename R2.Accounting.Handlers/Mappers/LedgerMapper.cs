using System.Collections.Generic;
using System.Linq;
using R2.Accounting.Domain;
using R2.Accounting.Dtos;

namespace R2.Accounting.Handlers.Mappers {
    public static class LedgerMapper {
        public static LedgerDto MapToDto (this Ledger ledger) {
            if (ledger == null) return null;
            return new LedgerDto {
                Id = ledger.Id,
                    Name = ledger.Name,
                    CreationTime = ledger.CreationTime,
                    CreatorUser = ledger.CreatorUser?.NickName ?? "",
                    LastModificationTime = ledger.LastModificationTime,
                    LastModifierUser = ledger.LastModifierUser?.NickName ?? ""
            };
        }

        public static List<LedgerDto> MapToDto (this ICollection<Ledger> ledgers) {
            if (ledgers == null) return new List<LedgerDto>();

            return ledgers.Select(c=>c.MapToDto()).ToList();
        }

    }
}