﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace R2.Accounting.Endpoint.Migrations
{
    public partial class InitDatabase : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TransactionCategory",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ConcurrencyStamp = table.Column<byte[]>(rowVersion: true, nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Icon = table.Column<string>(nullable: true),
                    IsSystemType = table.Column<bool>(nullable: false),
                    TransactionType = table.Column<int>(nullable: false),
                    ParentCategoryId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TransactionCategory", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TransactionCategory_TransactionCategory_ParentCategoryId",
                        column: x => x.ParentCategoryId,
                        principalTable: "TransactionCategory",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "EventSources",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ConcurrencyStamp = table.Column<byte[]>(rowVersion: true, nullable: true),
                    RequestType = table.Column<string>(nullable: true),
                    Request = table.Column<string>(nullable: true),
                    Succeeded = table.Column<bool>(nullable: false),
                    Response = table.Column<string>(nullable: true),
                    CreatorUserId = table.Column<string>(nullable: true),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    Duaration = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EventSources", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EventSources_AspNetUsers_CreatorUserId",
                        column: x => x.CreatorUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Ledger",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ConcurrencyStamp = table.Column<byte[]>(rowVersion: true, nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Balance = table.Column<decimal>(nullable: false),
                    CreatorUserId = table.Column<string>(nullable: true),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    LastModifierUserId = table.Column<string>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<string>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Ledger", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Ledger_AspNetUsers_CreatorUserId",
                        column: x => x.CreatorUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Ledger_AspNetUsers_DeleterUserId",
                        column: x => x.DeleterUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Ledger_AspNetUsers_LastModifierUserId",
                        column: x => x.LastModifierUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Account",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ConcurrencyStamp = table.Column<byte[]>(rowVersion: true, nullable: true),
                    LedgerId = table.Column<Guid>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    AccountNumber = table.Column<string>(nullable: true),
                    CardNumber = table.Column<string>(nullable: true),
                    Balance = table.Column<decimal>(nullable: false),
                    CreatorUserId = table.Column<string>(nullable: true),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    LastModifierUserId = table.Column<string>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    DeleterUserId = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Account", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Account_AspNetUsers_CreatorUserId",
                        column: x => x.CreatorUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Account_AspNetUsers_DeleterUserId",
                        column: x => x.DeleterUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Account_AspNetUsers_LastModifierUserId",
                        column: x => x.LastModifierUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Account_Ledger_LedgerId",
                        column: x => x.LedgerId,
                        principalTable: "Ledger",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "LedgerAccessRight",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ConcurrencyStamp = table.Column<byte[]>(rowVersion: true, nullable: true),
                    LedgerId = table.Column<Guid>(nullable: true),
                    UserId = table.Column<string>(nullable: true),
                    LedgerPermission = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LedgerAccessRight", x => x.Id);
                    table.ForeignKey(
                        name: "FK_LedgerAccessRight_Ledger_LedgerId",
                        column: x => x.LedgerId,
                        principalTable: "Ledger",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_LedgerAccessRight_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Transaction",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ConcurrencyStamp = table.Column<byte[]>(rowVersion: true, nullable: true),
                    AccountId = table.Column<Guid>(nullable: true),
                    TransactionType = table.Column<int>(nullable: false),
                    Value = table.Column<decimal>(nullable: false),
                    TransactionDateTime = table.Column<DateTime>(nullable: false),
                    Note = table.Column<string>(nullable: true),
                    CategoryId = table.Column<Guid>(nullable: true),
                    CreatorUserId = table.Column<string>(nullable: true),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    LastModifierUserId = table.Column<string>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    DeleterUserId = table.Column<string>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Transaction", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Transaction_Account_AccountId",
                        column: x => x.AccountId,
                        principalTable: "Account",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Transaction_TransactionCategory_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "TransactionCategory",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Transaction_AspNetUsers_CreatorUserId",
                        column: x => x.CreatorUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Transaction_AspNetUsers_DeleterUserId",
                        column: x => x.DeleterUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Transaction_AspNetUsers_LastModifierUserId",
                        column: x => x.LastModifierUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "TransactionCategory",
                columns: new[] { "Id", "Icon", "IsSystemType", "Name", "ParentCategoryId", "TransactionType" },
                values: new object[] { new Guid("083e1644-de62-4bbd-8606-6e31934e85c1"), null, true, "افتتاح حساب", null, 0 });

            migrationBuilder.CreateIndex(
                name: "IX_Account_CreatorUserId",
                table: "Account",
                column: "CreatorUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Account_DeleterUserId",
                table: "Account",
                column: "DeleterUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Account_LastModifierUserId",
                table: "Account",
                column: "LastModifierUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Account_LedgerId",
                table: "Account",
                column: "LedgerId");

            migrationBuilder.CreateIndex(
                name: "IX_EventSources_CreatorUserId",
                table: "EventSources",
                column: "CreatorUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Ledger_CreatorUserId",
                table: "Ledger",
                column: "CreatorUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Ledger_DeleterUserId",
                table: "Ledger",
                column: "DeleterUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Ledger_LastModifierUserId",
                table: "Ledger",
                column: "LastModifierUserId");

            migrationBuilder.CreateIndex(
                name: "IX_LedgerAccessRight_LedgerId",
                table: "LedgerAccessRight",
                column: "LedgerId");

            migrationBuilder.CreateIndex(
                name: "IX_LedgerAccessRight_UserId",
                table: "LedgerAccessRight",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Transaction_AccountId",
                table: "Transaction",
                column: "AccountId");

            migrationBuilder.CreateIndex(
                name: "IX_Transaction_CategoryId",
                table: "Transaction",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_Transaction_CreatorUserId",
                table: "Transaction",
                column: "CreatorUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Transaction_DeleterUserId",
                table: "Transaction",
                column: "DeleterUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Transaction_LastModifierUserId",
                table: "Transaction",
                column: "LastModifierUserId");

            migrationBuilder.CreateIndex(
                name: "IX_TransactionCategory_ParentCategoryId",
                table: "TransactionCategory",
                column: "ParentCategoryId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "EventSources");

            migrationBuilder.DropTable(
                name: "LedgerAccessRight");

            migrationBuilder.DropTable(
                name: "Transaction");

            migrationBuilder.DropTable(
                name: "Account");

            migrationBuilder.DropTable(
                name: "TransactionCategory");

            migrationBuilder.DropTable(
                name: "Ledger");

            migrationBuilder.DropTable(
                name: "AspNetUsers");
        }
    }
}
