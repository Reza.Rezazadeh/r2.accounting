﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using R2.Accounting.Domain;
using R2.Framework.Domain.Context;
using R2.Framework.Domain.Helpers;

namespace R2.Accounting.Endpoint
{
    public class AccountingDataContext : DbContext, IDataContext
    {
        private readonly IConfigurationRoot Configuration;
        public AccountingDataContext(IConfigurationRoot configuration)
        {
            Configuration = configuration;
        }
        public AccountingDataContext() { }
        public AccountingDataContext(DbContextOptions<AccountingDataContext> options) : base(options) { }

        public DbSet<EventSource> EventSources { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            // string connectionString = Configuration.GetConnectionString ("DefaultConnection");
            string connectionString = "Password=123@.com;Persist Security Info=True;User ID=sa;Initial Catalog=R2.Accounting;Data Source=.\\sqlexpress";
            optionsBuilder.UseSqlServer(connectionString, options =>
               options.MigrationsAssembly(typeof(AccountingDataContext).Assembly.FullName));
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.DefineTAggregateDbSets();
            modelBuilder.UseEntityTypeConfiguration();

            base.OnModelCreating(modelBuilder);
        }
    }
}