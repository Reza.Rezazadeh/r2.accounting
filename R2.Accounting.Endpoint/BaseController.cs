using System;
using System.Threading.Tasks;
using Newtonsoft.Json;
using R2.Framework.Infrastrcture.Bus;
using R2.Framework.Infrastrcture.Message;

namespace R2.Accounting.Endpoint
{
    public class BaseController
    {
        private readonly ICommandBus Bus;

        public BaseController(ICommandBus bus)
        {
            Bus = bus;
        }

        public async Task PostMessage<TIn, TOut>(TIn message)
        where TIn : class, ICommand, new()
        where TOut : class, IResult, new()
        {
            try
            {
                var result = await Bus.Send<TIn, TOut>(message);
                Console.WriteLine(JsonConvert.SerializeObject(result));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

    }
}