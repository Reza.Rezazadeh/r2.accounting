﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using R2.Accounting.Domain.Repository;
using R2.Framework.Domain.Context;
using R2.Framework.Domain.Repository;
using R2.Framework.Infrastrcture.Bus;
using R2.Framework.Infrastrcture.Message;
using R2.Framework.MB.Configurations;
using R2.Framework.MB.Extensions;
using R2.Framework.MB.Interfaces;

namespace R2.Accounting.Endpoint {
    static class Program {
        // private static MediatR.IMediator mediator;
        public static IConfigurationRoot Configuration;
        static async Task Main (string[] args) {
            Assembly.Load ("R2.Accounting");

            var services = new ServiceCollection ();

            Configuration = new ConfigurationBuilder ()
                .SetBasePath (Directory.GetParent (AppContext.BaseDirectory).FullName)
                .AddJsonFile ("appsettings.json", false)
                .Build ();
            services.AddSingleton (Configuration);
            InitDataBase (services);
            await InitBusAsync (services);
            services.BuildServiceProvider ();
            Console.WriteLine ("Press any key to exit");
            await Task.Run (() => Console.ReadLine ());

        }

        private static ServiceCollection InitDataBase (this ServiceCollection services) {
            string connectionString = Configuration.GetConnectionString ("DefaultConnection");
            var dataContextBuilder = new DbContextOptionsBuilder<AccountingDataContext> ()
                .UseSqlServer (connectionString);
            var context = new AccountingDataContext (dataContextBuilder.Options);

            services.AddDbContext<AccountingDataContext> (options => options.UseSqlServer (connectionString));
            services.AddSingleton (context);
            // services.AddScoped<IDataContext, DataContext> ();
            services.AddSingleton<IUnitOfWork, UnitOfWork<AccountingDataContext>> ();
            services.AddScoped (typeof (IRepository<>), typeof (Repository<>));
            services.AddScoped (typeof (IUserRepository), _ => new UserRepository (context));

            context.Database.Migrate ();

            return services;
        }

        private static async Task<ServiceCollection> InitBusAsync (ServiceCollection services) {
            try {
                Assembly.Load ("R2.Accounting.Handlers");
                Assembly.Load ("R2.Accounting.QueryHandlers");
                services.AddHandles ();

                var rabbitMqOptions = new RabbitMqOptions ();
                Configuration.GetSection (RabbitMqOptions.Position).Bind (rabbitMqOptions);

                services.AddCommandConsumers(rabbitMqOptions);

                var container = services.BuildServiceProvider ();

                var _commandConsumer = container.GetService<ICommandConsumerMessageBusManager> ();

                await _commandConsumer.Start ();
                await Console.Out.WriteLineAsync ("**********     Message Bus Started     **********");
                return services;
            } catch (Exception ex) {
                await Console.Out.WriteLineAsync ("**********     Message Bus Staring Failed     **********");

                await Console.Out.WriteLineAsync (ex.Message);
                throw;
            }

        }

        private static async Task InitSenderAndSend (ServiceCollection serviceProvider) {
            var rabbitMqOptions = new RabbitMqOptions ();
            Configuration.GetSection (RabbitMqOptions.Position).Bind (rabbitMqOptions);
            rabbitMqOptions.EndpointName = "R2.ShopNet.Sendeer";
            serviceProvider.RegisterCommandSender (null, rabbitMqOptions);

            var container = serviceProvider.BuildServiceProvider ();
            var _commandSender = container.GetService<ICommandSenderMessageBusManager> ();
            await _commandSender.Start ();

            // var bus = container.GetService<ICommandBus> ();
            // await bus.Send<CreateCategory, Result>(new CreateCategory {Name = "dfg"});

        }
    }
}