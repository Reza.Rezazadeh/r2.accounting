﻿using System;
using System.Linq;
using System.Threading.Tasks;
using R2.Accounting.Commands;
using R2.Accounting.Commands.Ledger;
using R2.Accounting.Domain;
using R2.Accounting.Handlers.Mappers;
using R2.Framework.Domain.Context;
using R2.Framework.Infrastrcture.Bus;

namespace R2.Accounting.Handlers {

    public class GetLedgerQueryHandler : MessageHandler<GetLedgerQuery, GetLedgerQueryResult> {
        private readonly IUnitOfWork unitOfWork;

        public GetLedgerQueryHandler (IUnitOfWork unitOfWork) {
            this.unitOfWork = unitOfWork;
        }
        public override async Task<GetLedgerQueryResult> Consume (GetLedgerQuery command) {
            var ledgerRepository = unitOfWork.GetRepository<Ledger> ();
            var ledger = ledgerRepository.Query(predicate: c=>c.IsDeleted == false && c.Id == command.Id).FirstOrDefault();
            return new GetLedgerQueryResult {
                Entity = ledger.MapToDto ()
            };
        }
    }
}