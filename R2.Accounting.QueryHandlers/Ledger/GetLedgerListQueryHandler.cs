﻿// using System;
// using System.Linq;
// using System.Linq.Expressions;
// using System.Threading.Tasks;
// using R2.Accounting.Commands;
// using R2.Accounting.Commands.Ledger;
// using R2.Accounting.Domain;
// using R2.Accounting.Handlers.Mappers;
// using R2.Framework.Domain.Context;
// using R2.Framework.Infrastrcture.Bus;

// namespace R2.Accounting.Handlers {

//     public class GetLedgerListQueryHandler : ListQueryHandler<GetLedgerListQuery, GetLedgerListQueryResult> {
//         private readonly IUnitOfWork unitOfWork;

//         public GetLedgerListQueryHandler (IUnitOfWork unitOfWork) {
//             this.unitOfWork = unitOfWork;
//         }
//         public override async Task<GetLedgerListQueryResult> Consume (GetLedgerListQuery command) {
//             var ledgerRepository = unitOfWork.GetRepository<Ledger> ();

//             var ledgers = await ledgerRepository
//                 .GetPagedListAsync (predicate: t => t.IsDeleted == false, pageIndex: command.Skip, pageSize: command.Take);
//             return new GetLedgerListQueryResult {
//                 TotalCount = ledgers.TotalCount,
//                     Entities = ledgers.Items.ToList ().MapToDto ()
//             };
//         }
//     }
// }